# springboot-learn

#### 项目介绍
springboot 学习。现阶段有springboot整合mybatis实现多数据源；

#### 使用说明

#####1: 模块:mybatis-dynamic-datasource(实现mybatis的动态数据源切换)
>相关类:
- com.gitee.ley.mybatis.aspect.DataSourceSwitchAspect:实现数据源动态切换的切面
- com.gitee.ley.mybatis.config.DruidDataSourceConfig:实现数据源的注入
- com.gitee.ley.mybatis.config.MybatisConfig: 实现mybatis的相关配置
- com.gitee.ley.mybatis.utils.DbContextHolder: 用于保存当前线程的数据源名称
- com.gitee.ley.mybatis.utils.DBTypeEnum: 数据源名称枚举,尽量是数据库名称

#####2:模块:springboot-redis(实现redis整合springboot以及redis缓存的自动化管理)
>相关类:
- com.gitee.ley.redis.config.RedisConfig:Redis的配置类
- com.gitee.ley.redis.service.RedisService:Redis缓存自动化实现
- com.gitee.ley.redis.utils.RedisUtils:Redis操作的工具类简化
package com.ley.springboot.aop.controller;

import com.ley.springboot.aop.annotation.NeedLogin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @GetMapping("/user")
    @NeedLogin
    public String login() {
        return "login success";
    }
}

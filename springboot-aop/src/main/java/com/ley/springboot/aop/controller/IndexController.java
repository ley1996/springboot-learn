package com.ley.springboot.aop.controller;

import com.ley.springboot.aop.annotation.NeedLogin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/index")
public class IndexController {

    @GetMapping("/user/{name}")
    @NeedLogin
    public String index(@PathVariable String name) {
        return name;
    }
}

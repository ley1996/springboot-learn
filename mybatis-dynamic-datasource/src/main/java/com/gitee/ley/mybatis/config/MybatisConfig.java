package com.gitee.ley.mybatis.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * mybatis configuration(建议使用配置类的方式重新声明相关bean)
 **/
@Configuration
@MapperScan(basePackages = {"com.gitee.ley.mybatis.dao"})
@AutoConfigureAfter(value = {DruidDataSourceConfig.class})
@Slf4j
public class MybatisConfig {


    @Value("${mybatis.mapper-locations}")
    private String mapperLocations;

    @Value("${mybatis.config-location}")
    private String configLocation;

    /**
     * 分离数据源的时候,尽量使用这种方式,不要直接注入数据源,可能会出现bean注入循环依赖
     **/
    @Autowired
    private DruidDataSourceConfig druidDataSourceConfig;

    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(druidDataSourceConfig.dynamicDataSource(druidDataSourceConfig.db1(), druidDataSourceConfig.db2()));
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        Resource[] mapperResources = resourcePatternResolver.getResources(mapperLocations);
        sqlSessionFactory.setMapperLocations(mapperResources);
        sqlSessionFactory.setConfigLocation(resourcePatternResolver.getResource(configLocation));
        log.info("config location: {},mapper locations: {}", configLocation, mapperLocations);
        return sqlSessionFactory.getObject();
    }


    @Bean("sqlSessionTemplate")
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }


    @Bean("batchSqlSessionTemplate")
    public SqlSessionTemplate batchSqlSessionTemplate(@Qualifier("sqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory, ExecutorType.BATCH);
    }


    /**
     * mybatis数据源事务管理
     **/
//    public DataSourceTransactionManager db1TransactionManager() {
//        return new DataSourceTransactionManager(druidDataSourceConfig.dataSource(druidDataSourceConfig.db1(), druidDataSourceConfig.db2()));
//    }

}

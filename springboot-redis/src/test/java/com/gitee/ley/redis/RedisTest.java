package com.gitee.ley.redis;

import com.gitee.ley.redis.service.RedisService;
import com.gitee.ley.redis.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RedisApplication.class})
@Slf4j
public class RedisTest {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private RedisService redisService;


    @Test
    public void testRedisUtils() {
        redisUtils.set("host", "localhost");
        log.info("host: {}", redisUtils.get("host"));
    }


    @Test
    public void testAutoDeleteCache() {
        redisService.deleteOne("f7da3d18-ad7e-4b6a-8056-4b6f234e8598");
    }

    @Test
    public void testAutoCache() {
        String id = UUID.randomUUID().toString();
        redisService.findOne(id);
        log.info("find one: {}", redisUtils.get(id));
    }
}

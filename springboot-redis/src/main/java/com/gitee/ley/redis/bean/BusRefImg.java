package com.gitee.ley.redis.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusRefImg {

    private String id;
    private String busId;
    private String imgId;

}

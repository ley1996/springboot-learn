package com.ley.springboot.mybatisplus.business.mapper;

import com.ley.springboot.mybatisplus.business.entity.Dorm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuenyuan
 * @since 2018-11-09
 */
public interface DormMapper extends BaseMapper<Dorm> {

}

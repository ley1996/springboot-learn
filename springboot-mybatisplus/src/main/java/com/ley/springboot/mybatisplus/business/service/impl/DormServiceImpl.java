package com.ley.springboot.mybatisplus.business.service.impl;

import com.ley.springboot.mybatisplus.business.entity.Dorm;
import com.ley.springboot.mybatisplus.business.mapper.DormMapper;
import com.ley.springboot.mybatisplus.business.service.DormService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuenyuan
 * @since 2018-11-09
 */
@Service("dormService")
public class DormServiceImpl extends ServiceImpl<DormMapper, Dorm> implements DormService {

}

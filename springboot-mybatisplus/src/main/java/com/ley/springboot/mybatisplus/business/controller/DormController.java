package com.ley.springboot.mybatisplus.business.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuenyuan
 * @since 2018-11-09
 */
@RestController
@RequestMapping("/business/dorm")
public class DormController {

}

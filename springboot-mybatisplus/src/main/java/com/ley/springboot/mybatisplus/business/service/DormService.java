package com.ley.springboot.mybatisplus.business.service;

import com.ley.springboot.mybatisplus.business.entity.Dorm;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuenyuan
 * @since 2018-11-09
 */
public interface DormService extends IService<Dorm> {

}

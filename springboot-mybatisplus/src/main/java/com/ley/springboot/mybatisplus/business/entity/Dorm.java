package com.ley.springboot.mybatisplus.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuenyuan
 * @since 2018-11-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_dorm")
public class Dorm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 宿舍表主键
     */
    private String dormId;

    /**
     * 宿舍号
     */
    private String dormNumber;


}

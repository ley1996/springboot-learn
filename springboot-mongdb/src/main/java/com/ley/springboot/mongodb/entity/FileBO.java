package com.ley.springboot.mongodb.entity;

import lombok.Data;

import java.util.Date;

/**
 * file bo
 **/
@Data
public class FileBO {

    private String fileId;

    private String fileName;

    private String fileSuffix;

    private Date uploadTime;
}

package com.ley.springboot.mongodb.service;

import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * mongodb service
 *
 * @author liuenyuan
 **/
@Service
@Slf4j
public class MongodbService {

    //获得SpringBoot提供的mongodb的GridFS对象
    @Autowired
    private GridFsTemplate gridFsTemplate;


    /**
     * store file to mongodb
     *
     * @param files the files store to mongodb
     **/
    public List<String> storeFile2Mongodb(MultipartFile... files) throws IOException {
        if (files != null && files.length > 0) {
            List<String> filePaths = new ArrayList<>(files.length);
            for (MultipartFile file : files) {
                //获取提交的文件名
                String fileName = file.getOriginalFilename();
                // 获得文件输入流
                InputStream ins = file.getInputStream();
                // 获得文件类型
                String contentType = file.getContentType();
                // 将文件存储到mongodb中,mongodb 将会返回这个文件
                GridFSFile gridFSFile = gridFsTemplate.store(ins, fileName, contentType);

                log.info("grid fs file: {}", gridFSFile);
            }
        }
        return null;
    }


    /**
     * store file to mongodb
     *
     * @param files the files store to mongodb
     **/
    public List<String> storeFile2Mongodb(File... files) throws IOException {
        if (files != null && files.length > 0) {
            List<String> filePaths = new ArrayList<>(files.length);
            for (File file : files) {
                FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    //获取提交的文件名
                    String fileName = file.getName();
                    // 将文件存储到mongodb中,mongodb 将会返回这个文件
                    GridFSFile gridFSFile = gridFsTemplate.store(fileInputStream, fileName);
                    log.info("grid fs file: {}", gridFSFile);
                } finally {
                    fileInputStream.close();
                }
            }
        }
        return null;
    }


    public List<byte[]> getFileFromMongodb(String... fileIds) {
        List<byte[]> fileList = new ArrayList<>();
        for (String fileId : fileIds) {
            Query query = Query.query(Criteria.where("filename").is(fileId));
            GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(query);
            log.info("gridFSDBFile: {}", gridFSDBFile);
            InputStream inputStream = gridFSDBFile.getInputStream();
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
                IOUtils.copy(inputStream, baos);
                fileList.add(baos.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileList;
    }
}

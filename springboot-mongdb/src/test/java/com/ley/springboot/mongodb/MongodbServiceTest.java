package com.ley.springboot.mongodb;

import com.ley.springboot.mongodb.dao.MongodbRepository;
import com.ley.springboot.mongodb.service.MongodbService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MongodbApplication.class})
public class MongodbServiceTest {

    @Autowired
    private MongodbService mongodbService;

    @Autowired
    private MongodbRepository mongodbRepository;

    @Test
    public void testMongodb() throws IOException {
        File file = new File("D:\\idea workspace\\springboot-learn\\springboot-mongdb\\src\\main\\resources\\application.properties");
        mongodbService.storeFile2Mongodb(file);
    }

    @Test
    public void testMongodbDownload() throws IOException {
        mongodbService.getFileFromMongodb("5bd6cc69bd1423015c64f7bc");
    }

    @Test
    public void testMongodbRepository() {
        System.out.println(mongodbRepository.findFileBOByFileId("5bd6cc69bd1423015c64f7bc"));

    }

}
